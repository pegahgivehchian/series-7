package sbu.cs;


import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

public class Server {

    /**
     * Create a server here and wait for connection to establish,
     *  then get file name and after that get file and save it in the given directory
     *
     * @param args an string array with one element
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        // below is the name of directory which you must save the file in it
        //String directory = args[0];     // default: "server-database"
        String directory = "database";
        ServerSocket serverSocket = new ServerSocket(8080);
        Socket socket = serverSocket.accept();
        OutputStream out = null;
        InputStream in =  socket.getInputStream();
        DataInputStream dataInputStream = new DataInputStream(in);
        String filePath = dataInputStream.readUTF();

        try {
            File f = new File(directory);
            if (f.mkdir()){
                System.out.println("directory created");
            }
            File f2 = new File(directory +"/"+ filePath );
            if(f2.createNewFile()){
                System.out.println("file created");
            }
            out = new FileOutputStream(f2);
        } catch (FileNotFoundException ex) {
            System.out.println("File not found. ");
        }

        byte[] bytes = new byte[16*1024];
        int count;
        while ((count = in.read(bytes)) > 0) {
            out.write(bytes, 0, count);
        }

        out.close();
        dataInputStream.close();
        in.close();
        socket.close();
        serverSocket.close();
    }
}
