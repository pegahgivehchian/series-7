package sbu.cs;

import java.io.*;
import java.net.Socket;

public class Client {

    /**
     * Create a socket and connect to server then send fileName and after that send file to be saved on server side.
     *  in here filePath == fileName
     *
     * @param args a string array with one element
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        String filePath = args[0];      // "sbu.png" or "book.pdf"
        String host = "127.0.0.1";
        Socket socket = new Socket(host, 8080);
        File file = new File(filePath);
        byte[] bytes = new byte[16 * 1024];
        InputStream in = new FileInputStream(file);
        OutputStream out = socket.getOutputStream();
        DataOutputStream outputStream = new DataOutputStream(out);

        outputStream.writeUTF(filePath);

        int count;
        while ((count = in.read(bytes)) > 0) {
            out.write(bytes, 0, count);
        }

        outputStream.flush();
        outputStream.close();
        out.close();
        in.close();
        socket.close();
    }
}
